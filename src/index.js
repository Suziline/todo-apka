import React, { useState, useEffect, useCallback } from 'react';
import ReactDOM from 'react-dom';
import axios from 'axios';
// import {Container, Row, Col} from 'react-bootstrap';

import { Header } from './components/Header'
import { TodoList } from './components/TodoList'
import { TodoForm } from './components/TodoForm'

import './styles.css'


const App = () => {

  const [todos, setTodos] = useState([])
  // const [filter, setFilter] = useState('All');
  const [editingId, setEditingId] = useState('');

  const loadTodos = useCallback(async () => {
    const response = await axios.get('http://localhost:3001/todos');
    setTodos(response.data)
  }, [])

  useEffect(() => {
   loadTodos(); 
  }, [loadTodos])
  
  

  const addTodo = async (todo) => {  
    const newTodo ={
      name: todo.name,
      description: todo.description,
      dueDate: todo.dueDate,
      completed: false
    }
    await axios.post('http://localhost:3001/todos', newTodo)
    loadTodos()
  }

  // const FILTER_MAP = {
  //   All: () => true,
  //   Active: newTodo => !task.completed,
  //   Completed: newTodo => task.completed
  // };

  const removeTodo = async (id) => {
    console.log('removeTodo called')
    console.log('with id ' + id)
    await axios.delete('http://localhost:3001/todos/' + id)
    loadTodos();
  }

  const completeTodo = async (id) => {
    console.log('completeTodo called with id ' + id);
    await axios.put('http://localhost:3001/todos/' + id, {completed: true})
    loadTodos();
  }

  const unCompletedTodo = async (id) => {
    console.log('unCompletedTodo called with id ' + id);
    await axios.put('http://localhost:3001/todos/' + id, {completed: false})
    loadTodos();
  }
  
  const removeAllClick = async () => {
    await axios.delete('http://localhost:3001/todos/delete/all/')
    loadTodos()

  }
  const startEditing = (todo) => {
    setEditingId(todo.id);
  }

  const getTodoOnId = () => {
    const todo = todos.find((todo) => todo.id === editingId);
    return todo;
  }

  

  const updateTodo = async (todo) => {
    console.log('updateTodo called with id ' + todo.id);
    await axios.put('http://localhost:3001/todos/' + todo.id, todo);
    setEditingId('');
    loadTodos();
  }


  return  (
    
    <div className="app">
      <div className="container"> 
        <Header title="TODO LIST" isVisible={true} subtitle='Co mozes urobit zajtra, nerob dnes!'></Header>
        <div className="content"> 
          <TodoForm onAdd = {addTodo}
                    todo = {getTodoOnId()}
                    onUpdate = {updateTodo}>
         </TodoForm>
         <TodoList
          todos={todos} 
          onRemove={removeTodo} 
          onComplete={completeTodo} 
          unComplete = {unCompletedTodo} 
          onEdit= {startEditing}
          onRemoveAll={removeAllClick}>
         </TodoList>     
        </div>
      </div>
    </div>
    
  )
}


ReactDOM.render(
    <App />,
    document.getElementById('root')
);