import React from 'react';
import ReactDOM from 'react-dom';

// const Header = (props) =>{
//   return (
//   <div>
//     <h1>{props.title}</h1>
//     <h2>{props.nazov}</h2>
//     </div> )
// }


const Header = (props) => {
  if(props.isVisible === true) {
   return <h1>{props.title}</h1>
  } else {
    return null;
  }

}

  const Subtitle = (props) => {
    return props.subtitle ? <h2>{props.subtitle}</h2> : null;
  }
  
  // [pozri ine sposoby zobrazovania subtitle]
  // const Subtitle = (props) => {
  //   const subtitle = props.subtitle;
  //   return subtitle ? <h2>{subtitle}</h2> : null;
  // }

  
  // const TodoLists =  (props) => {
  //   const todos = props.todos;
  //   if(todos.length === 0) {
  //     return <p>nothing to do</p>
  //    } else {
  //      return <p>Here is your list</p>
  //    }
  //    <div>
  //      {todos.map {

  //      }}
  //    </div>
  // }

  const Todo = (props) => {
    return <div>{props.todo}</div>
  }

//  const TodoList = (props) => { 
//    const todos = props.todos
//    return 
//    <div>
//     {todos.length === 0 ? <p> Here is your todo list</p>: <p>Nothing to do</p>} 
//     <div>
//    {todos.map(x => {
//         return(<p key={x}>{x}</p>)
//       })}  
// </div>
// </div>
// }




const info = {
  title: 'To do list',
  subtitle: 'Co mozes urobit zajtra, nerob dnes!',
  todos: ["obed", "vecera"]
}

const onFormSubmit = (e) => {
  e.preventDefault(); 
  console.log("form submited") 

const todo = e.target.elements.newtodo.value;
if (todo) {
  info.todos.push(todo);
  e.target.elements.newtodo.value = "";
  renderApp()
  // console.log(todo)
  // console.log(info.todos)
}
}

const removeAllClick = () => {
  info.todos = []
  renderApp();
  // console.log(info.todos)
}



const renderApp = () => {
const app = (
  <div>
    <Todo todos = {info.todos} />

    <Header title ={info.title} isVisible={true} />
    <Subtitle subtitle= {info.subtitle} />
    {/* {info.subtitle && <h2>{info.subtitle}</h2>}  */}


    {/* <Todo title = {info.todos} isVisible={false} />
    <h1>{info.title}</h1>
    
    {info.subtitle && <h2>{info.subtitle}</h2>} 

    {/* <p>{(info.todos.length > 0) ? 
    'Here is your todo list' : 
    'Nothing to do'}</p> */}

    {/* <p>{info.todos.length}</p> */}
    
    {/* <div>{
      info.todos.map(x => {
         return(<p key={x}>{x}</p>)
      })   
      }
    </div> */}
    <form onSubmit={onFormSubmit}>
      <input type = "text" name = "newtodo"></input>
      <button id = "button">Add TODO</button>
    </form>
    <button onClick={removeAllClick}>Remove All</button>
  </div>
)

ReactDOM.render(
    app,
  document.getElementById('root')
);
}
renderApp();