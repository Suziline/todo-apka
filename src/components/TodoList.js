import React from 'react'
import { Todo }  from './Todo'
import './TodoList.css';
// import {Container, Row, Col} from 'react-bootstrap';
let ID = 1;
const getId = () => ID++;


const TodoList = (props) => {
  const todos = props.todos;
  if (todos.length === 0) {
    return (<div className="todo-nothing"><p>Nothing to do</p></div>);
  }
  return (
    <div className="todo-list">
      <div className="todo-header">
        <h4>Here is your {todos.length} todo's:</h4>
        {/* <div>Total number of tasks: {todos.length}</div> */}
        <button className="button button-removeAll" onClick={() => props.onRemoveAll()}>Remove all</button>
      </div>
      {todos.map((todo) => {
        return (
          <div className="todo-row" key={getId()}>
            <Todo todo={todo} ></Todo>
            <div>
              {!todo.completed &&<button className="button button-done" onClick={() => props.onComplete(todo.id)}>Done</button>}
              {todo.completed &&<button className="button button-done" onClick={() => props.unComplete(todo.id)}>back to task</button>}
              
              <button className="button button-edit" onClick={() => props.onEdit(todo.id)}>Edit</button>
              <button className="button button-del" onClick={() => props.onRemove(todo.id)}>x</button>
            </div>              
          </div>
        )
      })}
    </div>
  )
}

export { TodoList }

  //  const TodoList = (props) => { 
  //    const todos = props.todos
  //    return 
  //    <div>
  //     {todos.length === 0 ? <p> Here is your todo list</p>: <p>Nothing to do</p>} 
  //     <div>
  //    {todos.map(x => {
  //         return(<p key={x}>{x}</p>)
  //       })}  
  // </div>
  // </div>
  // }