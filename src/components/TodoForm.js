// import 'bootstrap/dist/css/bootstrap.min.css';
// import Button from 'react-bootstrap/Button';
import './TodoForm.css';
import React, { useEffect, useState } from 'react'


const TodoForm = (props) => {
 
  const [name, setName] = useState(' ');
  const [description, setDescription] = useState( '');
  const [dueDate, setDueDate] = useState('');

  
  useEffect(() => {
    if(props.todo)  {
    setName(props.todo.name);
    setDescription(props.todo.description);
    setDueDate(props.todo.dueDate);
    }
  }, [props.todo]);

  const onFormSubmit = (e) => {
    console.log("form submited") 
    e.preventDefault(); // prevent full page refresh
    if (props.todo === undefined) {
      props.onAdd({name, description, dueDate})
    } else {
      props.onUpdate({id: props.todo.id, name, description, dueDate})
    }
    setName('')
    setDescription('')
    setDueDate('')
  }

     // console.log("form submited") 
      // e.target.elements.newtodo.value = "";
    
    // const todo = e.target.elements.newtodo.value;
    // if (todo) {
    //   props.onAdd(todo)
    //   e.target.elements.newtodo.value = "";
    //  }
    // }
    
  
    return (
      <div className = "todo-form">
        <h4>{props.todo ? 'Edit' : 'Add'}TODO</h4>
      <form onSubmit={onFormSubmit}>
          <input required className = "form-input" type="text" value={name} 
          onChange={(e) => setName(e.target.value)} placeholder= "New task"></input>
          <textarea className = "form-textarea" value={description} 
          onChange={(e) => setDescription(e.target.value)} placeholder= "...more details"></textarea>
          <input required type = "date" value={dueDate} onChange={(e) => setDueDate(e.target.value)}></input>
          <button className =  "button button-add" disabled = {!name || !dueDate}>{props.todo ? 'Edit' : 'Add'} Todo</button>
      </form>
      </div>
    )
    
    }
 export {TodoForm}
  


//  <div className="todo-form">
//  <h4>Add TODO</h4>
//  <form onSubmit={onFormSubmit}>
//    <input className="form-input" type="text" name="newtodo" value={name} onChange={(e) => setName(e.target.value)} placeholder="Co budes robit?"/>
//    <textarea className="form-text" value={description} onChange={(e) => setDescription(e.target.value)} placeholder="...pridaj par detailov"></textarea>
//    <button className="button button-add">Add TODO</button>      
//  </form>
// </div>
// )
// }
