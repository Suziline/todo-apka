import React from 'react';

import './Todo.css'

const Todo = (props) => {
  return (
  <div className = "todo">
    <div className = "todo-nd">
    <h5 className = "todo-name">{props.todo.name}</h5>
    <p className = "todo-description">{props.todo.description}</p>
    </div>
    <div className = "todo-date">
    <p>{props.todo.dueDate}</p>
    </div>
    {props.todo.completed && <p className = "todo-done">Done</p>}
    {/* {props.todo.completed && } */}
  </div>)
}
export {Todo}




// const Todo = (props) => {
//   return (
//   <div className = "todo">  
//     <h5 className = "todo-name">{props.todo.name}</h5>
//     <p className = "todo-description">{props.todo.description}</p>
//     <p>{props.todo.dueDate}</p>
//     {props.todo.completed && <p className = "todo-done">Done</p>}
//   </div>)
// }
// export {Todo}
